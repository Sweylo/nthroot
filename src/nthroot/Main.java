/*******************************************************************************

Copyright (c) 2016, Logan Matthew Nichols

Permission to use, copy, modify, and/or distribute this software for any 
purpose with or without fee is hereby granted, provided that the above 
copyright notice and this permission notice appear in all copies.

THE SOFTWARE IS PROVIDED "AS IS" AND THE AUTHOR DISCLAIMS ALL WARRANTIES WITH 
REGARD TO THIS SOFTWARE INCLUDING ALL IMPLIED WARRANTIES OF MERCHANTABILITY AND 
FITNESS. IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR ANY SPECIAL, DIRECT, 
INDIRECT, OR CONSEQUENTIAL DAMAGES OR ANY DAMAGES WHATSOEVER RESULTING FROM LOSS 
OF USE, DATA OR PROFITS, WHETHER IN AN ACTION OF CONTRACT, NEGLIGENCE OR OTHER 
TORTIOUS ACTION, ARISING OUT OF OR IN CONNECTION WITH THE USE OR PERFORMANCE OF 
THIS SOFTWARE.

*******************************************************************************/

package nthroot;

import java.math.BigDecimal;
import java.math.BigInteger;

public class Main {

	public static void main(String[] args) {
		
		// setup new NthRoot object for the square root of 2
		NthRoot root = new NthRoot(
			new BigDecimal("2"),		// the operand
			2,							// the order of the root
			NthRoot.OUTPUT_PROGRESS		// the output mode
		);
		
		// estimate the value to 100 decimal places
		BigDecimal ans = root.estimate(new BigInteger("100"));
		long elapsed = root.getElapsedTime();
		
		System.out.println("Ans = " + ans);
		System.out.println("Elapsed time = " + elapsed + " ns");
		
	}
	
}
