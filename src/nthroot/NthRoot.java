/*******************************************************************************

Copyright (c) 2016, Logan Matthew Nichols

Permission to use, copy, modify, and/or distribute this software for any 
purpose with or without fee is hereby granted, provided that the above 
copyright notice and this permission notice appear in all copies.

THE SOFTWARE IS PROVIDED "AS IS" AND THE AUTHOR DISCLAIMS ALL WARRANTIES WITH 
REGARD TO THIS SOFTWARE INCLUDING ALL IMPLIED WARRANTIES OF MERCHANTABILITY AND 
FITNESS. IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR ANY SPECIAL, DIRECT, 
INDIRECT, OR CONSEQUENTIAL DAMAGES OR ANY DAMAGES WHATSOEVER RESULTING FROM LOSS 
OF USE, DATA OR PROFITS, WHETHER IN AN ACTION OF CONTRACT, NEGLIGENCE OR OTHER 
TORTIOUS ACTION, ARISING OUT OF OR IN CONNECTION WITH THE USE OR PERFORMANCE OF 
THIS SOFTWARE.

*******************************************************************************/

package nthroot;

import java.math.BigDecimal;
import java.math.BigInteger;

public class NthRoot {
	
    private BigDecimal operand;
    private int rootOrder;
    private long elapsedTime;
    private int outputMode;

    // define output mode constants
    public static final int OUTPUT_NONE = 0;
    public static final int OUTPUT_PROGRESS = 1;
    public static final int OUTPUT_PROGRESS_VALUES = 2;

    // define comparison constants
    private static final int LESS = -1;
    private static final int MORE = 1;
    private static final int EQUAL = 0;

    // regular constructor
    public NthRoot(BigDecimal o, int ro) {
        operand = o;
        rootOrder = ro;
        outputMode = OUTPUT_NONE;
    }

    // constructor including output mode parameter
    public NthRoot(BigDecimal o, int ro, int om) {
        operand = o;
        rootOrder = ro;
        outputMode = om;
    }

    public BigDecimal estimate(BigInteger digits) {

        BigDecimal root = BigDecimal.ZERO;

        boolean perfectRoot = false;

        long startTime = System.nanoTime();
        digits = digits.add(BigInteger.ONE);

        while (
            root.pow(rootOrder).compareTo(operand) == LESS
            || 
            root.pow(rootOrder).compareTo(operand) == EQUAL
        ) {
            if (root.pow(rootOrder).compareTo(operand) == EQUAL) {
                perfectRoot = true;
                break;
            } else {
                root = root.add(BigDecimal.ONE);
            } 
        }

        if (!perfectRoot) {

            root = root.subtract(BigDecimal.ONE);

            for (
                BigInteger i = BigInteger.ONE; 
                i.compareTo(digits) == LESS; 
                i = i.add(BigInteger.ONE)
            ) {

                if (
                    outputMode == OUTPUT_PROGRESS 
                    || 
                    outputMode == OUTPUT_PROGRESS_VALUES
                ) {
                    System.out.printf(
                        "%s / %s\n", 
                        i, 
                        digits.subtract(BigInteger.ONE)
                    );
                }

                BigDecimal iPow = BigDecimal.TEN.pow(i.intValue());

                for (
                    BigDecimal j = BigDecimal.ONE; 
                    j.compareTo(BigDecimal.TEN.add(BigDecimal.ONE)) == LESS; 
                    j = j.add(BigDecimal.ONE)
                ) {

                    BigDecimal temp = root.add(j.divide(iPow));

                    if (outputMode == OUTPUT_PROGRESS_VALUES) { 
                        System.out.printf(
                            "%s | %s | %s", 
                            j, 
                            temp, 
                            temp.pow(rootOrder)
                        );
                    }

                    if (
                        temp.pow(rootOrder).compareTo(operand) == MORE 
                        || 
                        j.equals(BigDecimal.TEN)
                    ) {
                        root = root.add(
                            j.subtract(BigDecimal.ONE).divide(iPow)
                        );
                        break;
                    }

                }

            }

        }

        elapsedTime = (System.nanoTime() - startTime);

        return root;

    }

    public long getElapsedTime() {
        return elapsedTime;
    }

    public BigDecimal getOperand() {
        return operand;
    }

    public void setOperand(BigDecimal operand) {
        this.operand = operand;
    }

    public int getRootOrder() {
        return rootOrder;
    }

    public void setRootOrder(int rootOrder) {
        this.rootOrder = rootOrder;
    }

    public void setOutputMode(int outputMode) {
        this.outputMode = outputMode;
    }
    
}
